<#assign now = .now>
<#assign utc = now?iso_utc?datetime("yyyy-MM-dd'T'HH:mm:ss'Z'")>
{
    "Events": [
        {
        "SerialNum": "1234567890",
        "DeviceName": "${event.deviceID}",
        "User": "${event.userID}",
        "Timestamp": "${event.time!now?string["HHmmss"]}",
        "GPSLatitude": "?",
        "GPSLongitude": "?",
        "GPSSpeed": "?",
        "GPSTimestamp": "20191122114531",
        "Type": "SSC",
        "RunId": "${event.runID}",
        "StopId": "${event.runID}_${event.postalCode}_${event.houseNumber}__1000_1200",
        "ActionId": "",
        "ActionType": "",
        "ItemType": "",
        "ItemId": "",
        "Status": "15",
        "IncidentId": "",
        "Comment": "",
        "PhotoFileName": "",
        "TimerId": "",
        "ExtraParams": ""
        }]
}