---
Test
---
|import                                  |
|nl.praegus.fitnesse.slim.fixtures.sikuli|

|script         |sikuli test      |
|set record mode|true             |
|click          |vysor_icon       |
|click          |slack-icon       |
|click          |praegus          |
|click          |toolchain-channel|
|click          |message-toolchain|
|press          |Niceee!          |

