---
Test
---
|script                                                                                                                                              |
|click |Support                            |                                                                                                         |
|click |New                                                                                                                                          |
|select|Bizzomate                          |for                     |Customer                                                                        |
|ensure|is visible                         |No active SLA found for Bizzomate.                                                                       |
|click |Okay                                                                                                                                         |
|check |value of                           |Project                 |Bizzo App                                                                       |
|select|Low                                |for                     |Priority                                                                        |
|ensure|is visible                         |Non critical issues that have medium to low impact on the business, feature requests, development issues.|
|select|Support                            |for                     |Ticket type                                                                     |
|enter |!today (yyyyMMddHHmmss) Test ticket|as                      |xpath=//h5[text()='Subject']/following-sibling::*//input                        |
|enter |This is a test                     |as                      |xpath=//h5[text()='Description']/following-sibling::*//textarea                 |

|script                                 |
|click           |Add                   |
|click           |Ticket aanmaker       |
|click           |Select                |
|click           |Save                  |
|wait for visible|css=.ticketDescription|

|script                                                   |
|$ticketNr=|value of   |css=.ticketDescription h2         |
|$ticketNr=|replace all|.*#([0-9]+).*|in|$ticketNr|with|$1|
|click     |Close                                         |

|script                                                                                                                 |
|set search context to|css=[title^='My Open Tickets']                                                                   |
|enter                |$ticketNr|as     |xpath=//label[text()='Ticket number']/parent::div/following-sibling::div//input|
|click                |Search                                                                                           |
|check                |value of |Subject|in row where       |#       |is       |$ticketNr      |=~/.*Test ticket$/      |
|clear search context                                                                                                   |